#pragma once

#include <Eigen/Dense>
#include <ros/ros.h>
#include <string>
#include <std_msgs/String.h>
#include <geometry_msgs/Twist.h>


class HelloParameter {

	public:

		HelloParameter(ros::NodeHandle& nodeHandle) ;
		virtual ~HelloParameter();

	protected:
		bool readParameters() ;

	private:
		// ROS node handle.
		ros::NodeHandle& node_handle_ ;

		// ROS publisher
		ros::Publisher twist_publisher_ ;

		// variables from yaml
		std::string twist_topic_name_ ;
		double k_att_ ;
		Eigen::VectorXd default_joint_angles_ ;
};
