#include <ros/ros.h>
#include "hello_parameter/HelloParameter.hpp"

int main(int argc, char** argv) {

	// Initialize ROS
	ros::init(argc, argv, "hello_parameter");

	// Add a node handle
	ros::NodeHandle nodeHandle("~");

	// specify the frequency
	double param_loop_rate ;
	if ( !nodeHandle.getParam("/looprate", param_loop_rate )) {
		ROS_INFO("Cannot read parameter looprate, set to default value" );
		param_loop_rate = 1000 ;
	}

	ros::Rate loopRate(param_loop_rate) ;
	// Make an instance of your ROS package
	HelloParameter HelloParameter(nodeHandle);

	while (true ) {

		// call all the callbacks waiting to be called
		ros::spinOnce() ;

		// sleep for any time remaining to the publish rate
		loopRate.sleep() ;
	}

	return 0;
}
