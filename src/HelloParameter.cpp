#include "hello_parameter/HelloParameter.hpp"


HelloParameter::HelloParameter(ros::NodeHandle& nodeHandle) : node_handle_(nodeHandle) {

	if ( !readParameters() ) {
		ROS_INFO("Cannot read parameters");
	}
	else {
		ROS_INFO("Successfully read parameters");
	}

	twist_publisher_ = node_handle_.advertise<geometry_msgs::Twist>(twist_topic_name_, 3) ;


	ROS_INFO("Successfully initialize the node");
}

HelloParameter::~HelloParameter() {
}


bool HelloParameter::readParameters() {

	// reading the local parameter
	if ( !node_handle_.getParam("twist_topic_name", twist_topic_name_ )) {
		ROS_INFO("Cannot read twist_topic_name" );
		return false ;
	}

	// reading the local parameter
	if ( !node_handle_.getParam("planner/gains", k_att_ )) {
		ROS_INFO("Cannot read planner/gains" );
		return false ;
	}

	// reading an array
	std::vector<double> param_list ;
	param_list.clear() ;
	if ( node_handle_.getParam("joint/target", param_list )) {

		int param_size = param_list.size()  ;
		default_joint_angles_ = Eigen::VectorXd::Zero(param_size) ;
		for ( int i = 0; i < param_size; i++) {
			default_joint_angles_(i) = param_list[i] ;
		}

	}
	else {
		ROS_ERROR ("Cannot read joint/target" ) ;
		return false ;
	}



	// reading the local parameter
	std::string global_param ;
	std::string local_param ;
	if ( !node_handle_.getParam("/global_param_in_launch", global_param )) {
		ROS_INFO("Cannot read /global_param_in_launch" );
		return false ;
	}

	// reading the local parameter
	if ( !node_handle_.getParam("local_param_in_launch", local_param )) {
		ROS_INFO("Cannot read local_param_in_launch" );
		return false ;
	}



	ROS_INFO("twist_topic_name_: %s", twist_topic_name_.c_str() );
	ROS_INFO("k_att_: %f", k_att_ );
	ROS_INFO_STREAM( "default_joint_angles_: " << default_joint_angles_.transpose() ) ;

	ROS_INFO_STREAM( "/global_param_in_launch: " << global_param ) ;
	ROS_INFO_STREAM( "local_param_in_launch: " << local_param ) ;

	return true;
}
